import footer from "./footer";
import header from "./header";
import mainDisplay from "./mainDisplay";

export default { footer, header, mainDisplay };
